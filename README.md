## Semana 25 - Ajax

1. Intro Ajax
2. Quiz
3. Setup del proyecto
4. Nuestro primer Ajax (Selector y evento)
5. Jquery Snippets
6. Llamando Ajax y creación
7. Quiz
8. Mostrando el resultado
9. Quiz
10. Respuesta como JSON
11. Quiz
12. Formulario via AJAX
13. Form serialize
14. Formulario por UJS
15. QUiz
16. Integrando un buscador sin AJAX
17. Buscador con AJAX
18. Actualizando la tabla
19. Negociando contenido con RAILS
20. Votos por AJAX
21. Actualizando el resultado
22. Actualizando el voto con render de vista parcial
23. Quiz
